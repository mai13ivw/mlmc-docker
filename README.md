# mlmc-docker

This repository exists to turn mlmc models into servable docker API's.

For this to work with a trained model you'll need to execute the following command:

Create a folder that contains the model you want to use, the file should be the only file in the folder.
We assume the folder with the model is called /path/to/model
```bash
docker run -p 127.0.0.1:8000:8000 -v/path/to/model/:/model  jaborst/mlmc-docker-api
```