FROM pytorch/pytorch:1.7.0-cuda11.0-cudnn8-runtime
RUN apt-get update && apt-get install -y git
RUN  pip install melmac git+https://git.informatik.uni-leipzig.de/mai13ivw/mlmc-lab.git
COPY . /data/code/
RUN cd /data/code && pip install .
ENTRYPOINT python -m mlmc-docker.api