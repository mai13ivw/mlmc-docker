from setuptools import setup

setup(
    name='mlmc-docker',
    version='0.1',
    packages=['mlmc-docker'],
    url='',
    license='',
    author='Janos Borst',
    author_email='borst@informatik.uni-leipzig.de',
    description='This package turns mlmc models into a serving API docker',
    install_requires = [
        "sanic"
    ]
)
