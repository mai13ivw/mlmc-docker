import argparse
import pathlib
import mlmc
from sanic import Sanic
from sanic import response
import aiofiles, os
from datetime import datetime
from pathlib import Path
from sanic_cors import CORS, cross_origin

MODEL=mlmc.models.SimpleEncoder(classes={}, target="single", finetune="all")

app = Sanic(name="mlmc-docker")
app.config["UPLOAD"] = Path("/tmp/mlmc-upload")
CORS(app)

@app.route("/labels", methods=["POST", "GET"])
def set_labels(request):
    print(request.json)
    try:
        MODEL.create_labels(request.json["labels"])
        return response.HTTPResponse(body=None, status=200, headers=None, content_type=None)
    except Exception as e:
        return response.HTTPResponse(body=e, status=500, headers=None, content_type=None)

@app.route("/", methods=["POST"])
def predict(request):
    r = MODEL.predict(request.json["text"], return_scores=True)
    return response.json(r)

def secure_filename(x):
    """ToDo: Implementation!"""
    return x

async def write_file(path, body):
    async with aiofiles.open(path, 'wb') as f:
        await f.write(body)
    f.close()

def valid_file_size(file_body):
    if len(file_body) < 10485760:
        return True
    return False
def valid_file_type(file_name, file_type):
 # file_name_type = file_name.split('.')[-1]
 # if file_name_type == "pdf" and file_type == "application/pdf":
 #     return True
 return True
@app.route("/api/upload", methods=['POST'])
async def process_upload(request):
    print("upload triggered")
    # Create upload folder if doesn't exist

    if not app.config["UPLOAD"].exists():
        app.config["UPLOAD"].mkdir(parents=True)

    print(request.files)
    # Ensure a file was sent
    upload_file = request.files.get('file')
    if not upload_file:
        print("Files was not sent")
        return response.redirect("/?error=no_file")

    # Clean up the filename in case it creates security risks
    filename = secure_filename(upload_file.name)

    # Ensure the file is a valid type and size, and if so
    # write the file to disk and redirect back to main
    if not valid_file_type(upload_file.name, upload_file.type):
        print("File type invalid")
        return response.redirect('/?error=invalid_file_type')
    elif not valid_file_size(upload_file.body):
        print("Filesize invalid")
        return response.redirect('/?error=invalid_file_size')
    else:
        file_path = f"{ app.config['UPLOAD']/filename}"
        await write_file(file_path, upload_file.body)
        return response.redirect('/?error=none')

    return response.json(True)

@app.route("api/list_files", methods=["GET", "POST"])
def get_uploaded_files(request):
    return response.json({"files":[str(x.name) for x in app.config["UPLOAD"].glob("*") if x.is_file()]})


#--------------------------------------------
# Main
#--------------------------------------------------------
def main(host="0.0.0.0", port=8000, cert=None, key=None):
    # Start the Server
    if cert is not None and key is not None:
        import ssl
        context = ssl.create_default_context(purpose=ssl.Purpose.CLIENT_AUTH)
        context.load_cert_chain(certfile=cert,
                            keyfile=key)

        app.run(host=host, port=port, ssl=context)
    else:
        app.run(host=host, port=port)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Start a classification Server.')

    parser.add_argument('--host', default="0.0.0.0", metavar='host', type=str, help='host ip ')
    parser.add_argument('--port', default=8000, dest='port', type=int,  help='Port for the server')
    parser.add_argument('--cert', default=None, dest='cert', help='Path to the ssl-certificate')
    parser.add_argument('--key',  default=None, dest="key", help='Path to the ssl-key')
    parser.add_argument('--model', default="/model",  dest="model", help='Path to the ssl-key')
    parser.add_argument('--device',  default="cpu", dest="device", help='Path to the ssl-key')

    args = parser.parse_args()

    # model_path = pathlib.Path(args.model) if pathlib.Path(args.model).is_file() else list(pathlib.Path(args.model).glob("*"))[0]
    # MODEL = mlmc.load(pathlib.Path(args.model))
    main(args.host, args.port, args.cert, args.key)