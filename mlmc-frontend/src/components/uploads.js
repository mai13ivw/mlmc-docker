import React from "react";




class FileUpload extends React.Component {
    constructor() {
        super();
        this.state = {
            name: "file-path",
            selectedFile: null
        }
    }
    setName(name) {
        this.setState({name: name})
    }
    setSelectedFile(selectedFile) {
        this.setState({selectedFile: selectedFile})
    }
    render() {
        return (
            <form>
                <input
                    type="text"
                    placeholder={"Path to your file..."}
                    className="tags-input-container"
                    value={this.state.name}
                    onChange={(e) => this.setName(e.target.value)}
                />
                <button
                    value={this.state.selectedFile}
                >upload
                </button>
            </form>
        )
    }
}
class TextUpload extends React.Component{
    constructor() {
        super();
    }
    render(){
        return <div>Text Upload</div>
    }
}

class UploadForm extends React.Component{
    constructor() {
        super();
        this.setState({
            fileChecked: true
        })
        this.setOption = this.setOption.bind(this)
    }
    setOption(x){
        console.log(this.state)
        this.setState({"fileChecked": !this.state.fileChecked})
    }
    render(){
        return <div>
        <form onSubmit={this.setOption}>
            <label>File<input type="checkbox" value={this.fileChecked} onChange={this.setOption} /></label>
     <button className="btn btn-default" type="submit">Save</button>test: {"It's " + this.state["fileChecked"]}
        </form>
            {(this.state.fileChecked)? (<FileUpload />) : (<TextUpload />)}</div>
    }
}


//
export {UploadForm};