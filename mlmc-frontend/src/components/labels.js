// src/components/TagsInput.js
import React, { useState } from 'react';


class TagsInput extends React.Component{
    constructor(prop) {
        super(prop);
        this.state = {
            tags: prop.default
        }
        this.handleKeyDown = this.handleKeyDown.bind(this)
    }
    setTags(x){
        this.setState({tags:x})
    }
    handleKeyDown(e){
        // If user did not press enter key, return
        if(e.key !== 'Enter') return
        // Get the value of the input
        const value = e.target.value
        // If the value is empty, return
        if(!value.trim()) return
        // Add the value to the tags array
        this.setTags([...this.state.tags, value])
        // Clear the input
        e.target.value = ''
        this.props.getTags([...this.state.tags, value])
    }
    removeTag(index){
        this.setTags(this.state.tags.filter((el, i) => i !== index))
        this.props.getTags(this.state.tags.filter((el, i) => i !== index))
    }

    render(){
        return (
            <div className="tags-input-container">
                { this.state.tags.map((tag, index) => (
                    <div className="tag-item" key={index}>
                        <span className="text">{tag}</span>
                        <span className="close" onClick={() => this.removeTag(index)}>&times;</span>
                    </div>
                )) }
                <input onKeyDown={this.handleKeyDown} type="text" className="tags-input" placeholder="Type something" />
            </div>
        )
    }
}

export default TagsInput