import React,  {Component} from "react";
import TextField from '@mui/material/TextField';

class Inference extends Component{
    constructor() {
        super();
        this.state = JSON.parse(window.localStorage.getItem('state')) || {
            prediction: "None yet...",
            text:"",
            labels:""
        }
        // this.setText = this.setText.bind(this)
    }
    setState(state) {
        super.setState(state);
        window.localStorage.setItem('state', JSON.stringify(this.state));
    }
    setText = (x) => {
        this.setState({text: x.target.value})
        // window.localStorage.setItem('state', JSON.stringify(state));
    }
    getLabel = () => {
        this.setState({labels: this.getAPIPrediction()})
    }

    getAPIPrediction(){
        var choices = ["Politics", "Religion", "Science"]
        var index = Math.floor(Math.random() * choices.length);
        this.setState({labels: choices[index]});
        return choices[index];

    }

    render () {
        return <div className="body-div">
            <div className="body-headline-div">
                <h1>Inference</h1>
            </div>
            <br/>
            <div className="body-content-div">
                <form>
                    <TextField onChange={this.setText} id="outlined-basic" style = {{width: "100%"}}
                               label="Input Text..." variant="outlined" value={this.state.text}/>
                    <button onClick={this.getLabel}>Predict</button>
                </form>
            </div>
            <div>{this.state.labels}</div>
            </div>
    }
}

export {Inference};