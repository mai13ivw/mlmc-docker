import * as React from "react";

import {
    Menu,
    MenuItem,
    MenuButton, MenuRadioGroup
} from '@szhsin/react-menu';
import '@szhsin/react-menu/dist/index.css';
import '@szhsin/react-menu/dist/transitions/slide.css';
import {Component} from "react";
import {Training} from "./training";
import {Inference} from "./inference";
import {API} from "./api";

class AppMenu extends Component {
    constructor(){
        super();
        this.state = ({item:"Training"})
        this.setItem = this.setItem.bind(this)
    }
    setItem(x){
        console.log(x)
        this.setState({item: x})
    }
    getTextColor(x){
        return x === this.state.item? "blue":"black"
    }
    getBodyObject(){
        const x = this.state.item
        if (x === "Training"){
            return <Training/>
        }
        if (x === "Inference"){
            return <Inference/>
        }
        if (x === "API"){
            return <API/>
        }
        return <div>{"Choose something from the menu!"}</div>
    }
    render(){
        return (
            <>
            <div name="menu-div">
        <Menu menuButton={<MenuButton >Open menu</MenuButton>} transition>
            <MenuRadioGroup value={this.state.textColor} onRadioChange={e => this.setItem(e.value)}>
            <MenuItem style={{color:this.getTextColor("Training")}} type="radio" value="Training">Training</MenuItem>
            <MenuItem style={{color:this.getTextColor("Inference")}} type="radio" value="Inference">Inference</MenuItem>
            <MenuItem style={{color:this.getTextColor("API")}} type="radio" value="API">API</MenuItem>
            </MenuRadioGroup>
        </Menu>
                </div>
                <body>
                    {this.getBodyObject()}
                </body>
                </>

     );
    }

}
export {AppMenu}