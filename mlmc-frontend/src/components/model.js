import React, {Component} from "react";
import Select from '@mui/material/Select';
import MenuItem from "@mui/material/MenuItem";
import TextField from "@mui/material/TextField";
import TagsInput from "./labels";
import {Button, FormGroup} from "@mui/material";

class Model extends Component {
    constructor() {
        super();
        this.state = {
            model: "Transformer",
            modelSelection: ["Transformer", "Siamese", "Entailment"],
            representation: "bert-base-uncased",
            tags:["Politics", "Religion"]
        }
        this.handleChangeDropdown = this.handleChangeDropdown.bind(this)
        this.handleChangeRepresentation = this.handleChangeRepresentation.bind(this)
        this.submit = this.submit.bind(this)
        this.getTags = this.getTags.bind(this)
    }

    createModelMenu() {
        return (this.state.modelSelection).map(item => (<MenuItem value={item}>{item}</MenuItem>))
    }

    handleChangeDropdown(x) {
        this.setState({model: x.target.value})
    }

    handleChangeRepresentation(x) {
        this.setState({representation: x.target.value})
    }
    submit(e){
        console.log("Loggin modelcard")
        console.log("model", this.state.model, "representation", this.state.representation)
        console.log("tags", this.state.tags)
    }
    getTags(x){
        this.setState({tags:x})
    }

    render() {
        return (
            <div className={"model-card"}>
                <h1>Model Card</h1>
                <FormGroup fullwidth onSubmit={this.submit}>
                    <div className={"card-element"}>
                        <Select
                            labelId="demo-simple-select-label"
                            id="model"
                            value={this.state.model}
                            label="Model"
                            onChange={this.handleChangeDropdown}>
                            {this.createModelMenu()}
                        </Select>
                        <p>Model</p>
                    </div>
                    <div className={"card-element"}>
                        <TextField id="representation" value={this.state.representation} onChange={this.handleChangeRepresentation}></TextField>
                        <p>Representation</p>
                    </div>
                    <div className={"card-element"}>
                        labels: <TagsInput key={"labels"} default={this.state.tags} getTags={this.getTags}/>
                    </div>
                    <Button variant={"text"} type="submit" value="Submit" onClick={this.submit}>Save Configuration</Button>
                </FormGroup>
            </div>
        )
    }
}

export {Model};