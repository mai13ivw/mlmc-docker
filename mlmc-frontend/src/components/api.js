import React, {Component} from "react";
import axios from 'axios';
import Select from '@mui/material/Select';
import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';
import FormGroup from '@mui/material/FormControl';
import TagsInput from "./labels";
import { Circles, Puff,BallTriangle } from 'react-loading-icons';
import {Model} from "./model"
import {Button} from "@mui/material";

class API extends Component {
    constructor() {
        super();
        // Initially, no file is selected
        this.state = {
            selectedFile: null,
            files: Array(["Nothing yet"]),
            selectedFileDropdown: "Nothing yet",
            submitted:false
        };
        this.getFileList = this.getFileList.bind(this)
        this.handleChangeDropdown = this.handleChangeDropdown.bind(this)
        this.isTable = this.isTable.bind(this)
        this.run = this.run.bind(this)
    }

    // On file upload (click the upload button)
    onFileUpload = () => {
        // Create an object of formData
        const formData = new FormData();

        // Update the formData object
        formData.append(
            "file",
            this.state.selectedFile,
            this.state.selectedFile.name
        );

        // Details of the uploaded file
        console.log(this.state.selectedFile);

        // Request made to the backend api
        // Send formData object
        axios.post("http://cuda2:8000/api/upload", formData);
    };

    onFileChange = event => {
        this.setState({selectedFile: event.target.files[0]});
    };

    do(x) {
        console.log("do this")
        console.log(x)
        this.setState({selectedFileDropdown: x.files[0]})
        return x
    }

    getFileList() {
        fetch('http://cuda2:8000/api/list_files')
            .then(response => {
                if (!response.ok) {
                    throw new Error(response.statusText)
                }
                return response.json()
            })
            .then(data => this.setState(this.do(data)));

        console.log(this.state)
    };

    componentDidMount() {
        this.getFileList();
    }

    createMenuItems() {
        console.log("Create menu items")
        console.log(this.state.files)
        console.log((this.state.files).map(item => (<MenuItem value={item}>{item}</MenuItem>)))
        return (this.state.files).map(item => (<MenuItem value={item}>{item}</MenuItem>))
    }

    handleChangeDropdown(x) {
        console.log(x.target.value)
        this.setState({selectedFileDropdown: x.target.value})
    }

    isTable() {
        return this.state.selectedFileDropdown.endsWith(".xlsx")
    }

    run(){
        console.log("This is now run by our server...")
        this.setState({submitted: true})
    }

    render() {

        return <div className="body-div">
            <div className="body-headline-div">
                <h1>API</h1>
            </div>
            <br/>
            <div className="body-content-div">
                <div style={{"marginBottom": "50px"}}>
                    <input type="file" onChange={this.onFileChange}/>
                    <button onClick={this.onFileUpload}>
                        Upload!
                    </button>
                </div>
                <div>

                    <Model/>

                    <FormGroup fullWidth onSubmit={this.submit}>
                        <InputLabel id="demo-simple-select-label">Uploaded Files</InputLabel>
                        <Select
                            style={{maxWidth: "80%"}}
                            labelId="demo-simple-select-label"
                                id="demo-simple-select"
                                value={this.state.selectedFileDropdown}
                                label="Uploaded Files"
                                onChange={this.handleChangeDropdown}
                        >
                            {this.createMenuItems()}
                        </Select><Button style={{maxWidth: '20%', maxHeight: '30px', minWidth: '20%', minHeight: '30px'}} type={"submit"} variant={"text"} onClick={this.run}> {this.state.submitted? <Puff stroke="#000000" strokeOpacity={.125} speed={.75} />: <div>Run!</div>}</Button>

                        {this.isTable() ? <div>x: </div> : <div></div>}{this.isTable() ?<TagsInput key={"x"} default={[]}/> : <div></div>}

                    </FormGroup>
                </div>

            </div>
        </div>
    }
}

export {API};