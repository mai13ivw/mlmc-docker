import './App.css';
import {TagsInput} from "./components/labels"
import {UploadForm} from "./components/uploads"
import React, { useState } from "react";
import {
    Menu,
    MenuItem,
    MenuButton, MenuRadioGroup
} from '@szhsin/react-menu';
import '@szhsin/react-menu/dist/index.css';
import '@szhsin/react-menu/dist/transitions/slide.css';
import {Component} from "react";
import {Training} from "./components/training";
import {Inference} from "./components/inference";
import {API} from "./components/api";
import { Button } from "@material-ui/core";
import { TbGridDots } from 'react-icons/tb';

class App extends Component{
    constructor(){
        super();
        this.state = ({item:"API"})
        this.setItem = this.setItem.bind(this)
    }
    setItem(x){
        console.log(x)
        this.setState({item: x})
    }
    getTextColor(x){
        return x === this.state.item? "blue":"black"
    }
    getBodyObject(){
        const x = this.state.item
        if (x === "Training"){
            return <Training/>
        }
        if (x === "Inference"){
            return <Inference/>
        }
        if (x === "API"){
            return <API/>
        }
        return <div>{"Choose something from the menu!"}</div>
    }
    render(){


    return (<>

            <div className="menu-div">

                <Menu menuButton={<Button variant="text"><TbGridDots size={40}/> [melmac] </Button>} transition>
                    <MenuRadioGroup value={this.state.textColor} onRadioChange={e => this.setItem(e.value)}>
                    <MenuItem style={{color:this.getTextColor("Training")}} type="radio" value="Training">Training</MenuItem>
                    <MenuItem style={{color:this.getTextColor("Inference")}} type="radio" value="Inference">Inference</MenuItem>
                    <MenuItem style={{color:this.getTextColor("API")}} type="radio" value="API">API</MenuItem>
                    </MenuRadioGroup>
                </Menu>

            </div>

            <div className="body-div">
                {this.getBodyObject()}
            </div>
        </>
    )
     }
  ;
}

export default App;
